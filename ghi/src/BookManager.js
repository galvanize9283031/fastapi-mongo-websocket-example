import { Outlet } from 'react-router-dom';
// import { useGetTokenQuery } from './app/api';
import { useGetTokenQuery } from './app/authApi';
import { useNavigate } from 'react-router-dom';
import Notification from './Notification';

function BookManager() {
  const navigate = useNavigate();
  const { data: tokenData } = useGetTokenQuery();
  if (!tokenData) {
    return (
      <div className="container">
        <Notification type="info">Loading...</Notification>
      </div>
    );
  } else if (!tokenData.account.roles.includes('librarian')) {
    navigate('/');
  }

  return (
    <div className="container">
      <div className="columns is-centered">
        <div className="column is-half">
          <Outlet />
        </div>
      </div>
    </div>
  )
}

export default BookManager;
