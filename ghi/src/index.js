import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Navigate, Routes, Route } from 'react-router-dom';
import { store } from './app/store';
import { Provider } from 'react-redux';
import Nav from './Nav';
import App from './App';
import BookForm from './BookForm';
import BookManager from './BookManager';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <Nav />
        <Routes>
          <Route path="/" element={<App />} />
          <Route path="/manage-books" element={<BookManager />}>
            <Route path="new" element={<BookForm />} />
            <Route path=":bookId" element={<BookForm />} />
            <Route path="*" element={<p>Please select a book</p>} />
          </Route>
          <Route path="*" element={<Navigate to="/" />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>
);
