import { useEffect } from 'react';
import { useGetBooksQuery } from './app/api';
import { useGetTokenQuery } from './app/authApi';
import Notification from './Notification';
import BookCard from './BookCard';
import { ReconnectingWebSocket } from './ReconnectingWebSocket';
import './App.css';

const socketUrl = `${process.env.REACT_APP_WS_HOST}/ws`;
const socket = new ReconnectingWebSocket(socketUrl);

function App() {
  const { data, isError, isLoading, refetch } = useGetBooksQuery();
  const { data: tokenData } = useGetTokenQuery();
  const isPatron = tokenData && tokenData.account && tokenData.account.roles.includes('patron');
  const accountId = tokenData && tokenData.account && tokenData.account.id;

  useEffect(() => {
    socket.addEventListener('message', ({ data }) => {
      if (data === 'refetch books') {
        refetch();
      }
    });
  }, []);

  return (
    <div className="container my-4">
      {isLoading
        ? <Notification>Loading data</Notification>
        : isError
          ? <Notification type="danger">
            Could not load book list.
            Please try again later.
          </Notification>
          : <div className="book-grid">
            {data.books.map(book => (
              <BookCard
                key={book.id}
                book={book}
                isPatron={isPatron}
                accountId={accountId} />
            ))}
          </div>
      }
    </div>
  );
}

export default App;
